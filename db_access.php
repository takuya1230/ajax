<?php

function h($var)  // HTMLでのエスケープ処理をする関数
{
    if (is_array($var)) {
      return array_map('h', $var);
    } else {
      return htmlspecialchars($var, ENT_QUOTES, 'UTF-8');
    }
}

/*
 * Author: Hitoshi ASANO
 *
 * 注意！！： 
 * これ以下のソースについては、トライデントコンピュータ専門学校 Webデザイン学科の授業に限り利用可能とする
 *   以下関数は、PHP上からSQL文を扱うことに主眼を置いたものであり、セキュリティ面で問題がある。
 *   Web開発基礎の課題用に用いることは問題ないが、各自のポートフォリオ等に用いてはならない。
 */
function execute_sql($sql, $datas = array()) {
    $sql = trim($sql);

    // SQL文チェック
    if (check_sql($sql) === false) {
        return "ERROR!";
    }

    $db_server = '127.0.0.1';
    $db_name   = 'Ajax';
    $dsn = "mysql:host={$db_server};dbname={$db_name};charset=utf8";
    $db_user = 'root';  // 
    $db_pass = 'root';  // 

    try {
    // MySQLデータベースに接続します。
      $db = new PDO($dsn, $db_user, $db_pass);
    // プリペアドステートメントのエミュレーションを無効にします。
      $db->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
    // エラーが発生した場合、例外がスローされるようにします。
      $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
      //echo 'データベースに接続しました';
    // 接続できない場合、PDOException例外がスローされるのでキャッチします。
    } catch (PDOException $e) {
      echo 'データベースに接続できませんでした 理由: ' . h($e->getMessage());
      exit;
    }

    try {
        $st = $db->prepare($sql);
        $place = 1;
        foreach($datas as $key => $data) {
            $st->bindValue($place, $data);
            $place++;
        }
        $st->execute();

        if (strpos($sql, 'SELECT') === 0) {
            // SELECT文の場合: 取得結果の2次元配列を返す
            return $st->fetchAll(PDO::FETCH_ASSOC);
        } elseif (strpos($sql, 'INSERT') === 0) {
            // INSERT文の場合: 追加したレコードのIDを返す
            return $db->lastInsertId();
        } else {
            // それ以外(UPDATE, DELETE): 影響のあった件数を返す
            return $st->rowCount();
        }
    } catch (PDOException $e) {
      echo 'データベースへの処理中にエラーが発生しました: ' . h($e->getMessage());
      exit;
    }
}

function check_sql($sql) {
    // 空でないか
    if (trim($sql) == '' || trim(trim($sql, ';')) == '' ) {
        return false;
    }
    // コメントが含まれていないか
    if (strpos($sql, '--') !== false) {
        return false;
    }
    // 複数のクエリを実行しようとしていないか
    $pos = strpos(trim($sql), ';');
    if ($pos !== false && $pos != (strlen(trim($sql)) -1)) {
        return false;
    }

    return true;
}
/* ?> ファイルがPHPスクリプトのみの記述で終わる場合、終了タグは省略が可能。 */
