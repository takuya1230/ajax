<?php

    require_once('db_access.php');
    
//    404にするやつ
//    http_response_code(200);

    $id = isset($_POST['id']) ? $_POST['id'] : "";
    $date = isset($_POST['date']) ? $_POST['date'] : "";

//    入れるデータが重複しないか見てくる
    $rows = execute_sql(
        'SELECT count(*) AS `cnt` FROM `Aj_table` WHERE `id` = ?;',
        array($id)
    );

    //    データをデータベースに入れる
    if($rows[0]['cnt'] == 0){
        $ret = execute_sql(
            'INSERT INTO `Aj_table` (`id`, `date`, `deleted`) VALUES (?, ?, ?);',
            array($id, $date, false)
        );
    }else{
//        重複していたら更新する
        $ret = execute_sql(
            'UPDATE `Aj_table` SET `date` = ? WHERE id = ?',
            array($date, $id)
        );
    }
    
//    ajaxに使用するやつ
    $result = array(
        "weather" => "今日の天気は曇り" ,
//        "id" => $id,
//        "date" => $date
    );

    echo json_encode($result);

?>