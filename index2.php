
<?php
    
    include_once('db_access.php');

    $dates = execute_sql('SELECT * FROM `Aj_table`;');
//    var_dump($dates);

    $last_id = execute_sql('SELECT MAX(`id`) AS last_id FROM `Aj_table`;');
    $last_id = $last_id[0]['last_id'];

?>

<!DOCTYPE html>
<html lang="ja">
<head>
    <meta charset="UTF-8">
    <title>webprogram04(php)</title>
    <script src="https://code.jquery.com/jquery-2.2.4.js" integrity="sha256-iT6Q9iMJYuQiMWNd9lDyBUStIq/8PuOW33aOqmvFpqI="crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.js" integrity="sha256-DI6NdAhhFRnO2k51mumYeDShet3I8AKCQf/tf7ARNhI="crossorigin="anonymous"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1/i18n/jquery.ui.datepicker-ja.min.js"></script>
    <script>
        
        $(function () {
            
            
            
            $('.datepicker').datepicker();

            $('.btnClone').on('click', function(){
                $(this).clone(true).insertAfter(this);
                $(this).clone(true).insertBefore('#date_1');
                
                //formタグの囲みの中に追加
//                $(this).clone(true).appendTo('form');
                $(this).clone(true).prependTo('form');
            });
            
            $('.save').on('click', function(){
                var id = $(this).data('id');
                $('#form1_id').val(id);
                var date = $('#date_' + id).val();
                $('#form1_date').val(date);
            });
            
            $(".datepicker, .datepr").on('change', function(){
                var id = $(this).attr('id');
                id = id.replace(/date_/g, "");
                $('#form1_id').val(id);
                var date = $(this).val();
                $('#form1_date').val(date);
            });
            
            var next_id = <?php echo ($last_id + 1) ?>;
            $('.append').on('click', function (){
                var tr = $('#copy_tr').clone(true);
                tr.attr('id', '');
                tr.find('.datepr').attr('id', 'date_' + next_id);
                tr.find('.datepr').datepicker();
                tr.find('.save').data('id', next_id);
                tr.find('.save').attr('data-id', next_id);
                tr.insertBefore(this);
                
                next_id++;
            });
            
            $(".datepicker, .datepr").on('change', function(){
                 var id = $('#form1_id').val();
                      var date = $('#form1_date').val();
                
//                  ajax通信
                  $.ajax({
                      url: 'ajax.php',
                      type: "POST",
                      data: {id: id, date: date},
                      dataType: "json"
                  }).done(function (json){
                      alert("OK");
                      alert(json.weather);
                      for(i in json){
                          console.log(json);
                      }
                  });  
                
            });    
            
        });
        
    </script>
    <link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1/themes/redmond/jquery-ui.css">
</head>
<body>
    <form action="" method="post">
       <table id="tb">
       <?php foreach($dates as $data): ?>
           <tr>
               <td><input type="text" class="datepicker" id="date_<?php echo $data['id'] ?>" value="<?php echo  strtotime($data['date']); ?>"></td>
               <td><input type="button" class="save" data-id="<?php echo $data['id'] ?>" value="保存"></td>
           </tr>
           
           
           
        <?php endforeach; ?>
       </table>
       
       <table style="display:none;">
           <tr id="copy_tr">
               <td><input type="text" class="datepr" id=""></td>
               <td><input type="button" class="save" value="保存"></td>
           </tr>
       </table>
       <input type="button" class="append" value="行追加">
<!--        ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
        <hr>
        <form action="" method="post" id="form1">
            <input type="text" name="date" value="" id="form1_id">
            <input type="text" name="date" value="" id="form1_date">
        </form>
        <input type="button" value="Ajax通信" id="ajax">
        
<!--        ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->      
    </form> 
</body>
</html>